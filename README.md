# Match My Bottle

## Sommaire

- [Présentation de l'application](Présentation-de-l'application)
- [Comment lancer l'application ?](Comment-lancer-l'application-?)
- [La page d'accueil](La-page-d'accueil)
- [Le quizz !](Le-quizz-!)
- [Création de compte](Création-de-compte)
- [Liste des favoris](Liste-des-favoris)

## Présentation de l'application

![logo](https://imagizer.imageshack.com/img923/4710/EdsMLI.png)

MatchMyBottle est le site internet pour les gens qui veulent faire la fête mais qui en ont marre de boire toujours la même chose. Si tu es en manque de créativité tu as juste à répondre à 3 petites questions et MatchMyBottle te sortira la boisson la plus adaptée pour ta soirée.

## Comment lancer l'application ?

Infos pratique :
- version java utilisé : 1.8
- version kotlin utilisé : 1.4.10
- version mariadb utilisé : 2.7.0
- version mvn utilisé : 3.6.3

### premiere méthode

1. Importer le projet dans IntelliJ IDEA en important le fichier "pom.xml" à la racine de ce répertoire

2. Exécuter votre DB mysql. Si vous avez docker, vous pouvez utiliser la commande suivante:

```
docker run --name mariadb --rm -e MYSQL_ROOT_PASSWORD=toor -e MYSQL_DATABASE=defaultdb -p 3306:3306 -v "`pwd`/initdb:/docker-entrypoint-initdb.d" mariadb
```

3. Si vous n'avez pas Docker, et que vous avez un serveur MariaDB custom, vérifiez bien que vos utilisateurs / mdp sont les bons par rapport au fichier de configuration (src/main/resources/application.properties), et exécutez les scripts présents dans le dossier initdb

4. Tous les scripts sql contenus dans le dossier initdb seront exécutés automatiquement lors du premier chargement de la DB.

5. Lancez l'application via IntelliJ, et vérifiez qu'elle fonctionne sur http://localhost:8080 (par défaut)

### deuxieme méthode

Dans le projet, il faut ouvrir une invite de commandes ou un git bash.

dans APi : 
``` 	
        $ mvn package 
		$ ls target/ 	
		$ java -jar target/demo-0.0.1-SNAPCHOT.jar
```
dans Front : 
```
        $ng serve 
```
## La page d'accueil

Vendre ou offrir des boissons alcoolisées à un mineur est strictement interdit. MatchMyBottle s'assure que tous ses utilisateurs soient des adultes de façon très sécurisée et fiable.

![acceuil](/uploads/e43b441834b3cc6ac43bbf19af8d9d54/250141245_264468965614536_2967214491959551525_n.png)

Si l'utilisateur est majeur il sera redirigé vers la page du quiz !

## Le quizz !

C'est sur cette page que toute la magie de MatchMyBottle opère. Il suffit de cocher de répondre aux questions du quiz d'appuyer sur le bouton "Find My Match".

![Quizz](/uploads/51c1881276ecc9ad207e8607de9e9b21/251292989_415517063375304_6233349430667223205_n.png)

Et une nouvelle page avec le résultat apparaîtra.

![resultat](/uploads/234b3a36f7106b039f984a778a78991c/251009139_183435520625706_1507472216207708147_n.png)

## Création de compte

Pour te connecter à un compte il suffit d'appuyer sur le bouton "connexion" (en haut à gauche) du quiz.
Tu arriveras sur la page de connexion. Tu peux par exemple utiliser l'email : "paul.harrohide@yahoo.fr" et Mdp : "password" pour accéder à un compte déjà créer.

![ConnexionCompte](/uploads/834b3761ba3bc2838febb4da4e666b74/250983733_178277941161743_8802987282448080505_n.png)

Sinon tu peux te créer ton propre compte en appuyant sur le bouton "création de compte" en haut à droite de la page création.

![CreationCompte](/uploads/a400ee1ef5d2b9bb40b0c59f114d93e2/250914382_291282992855507_2230422367120266936_n.png)

Pour te déconnecter de ta session il suffit d'appuyer sur l'icône de la porte à droite de ton prénom.

![QuizzCompteConnecté](/uploads/f852c3d8532306f027f768c384d1283b/250964897_686960672667928_2412006961382473076_n.png)

## Liste des favoris

Le compte permet de créer une liste de ses favoris pour garder les meilleurs alcools/citations en mémoire.
Lorsque tu es connecté à ton compte tu peux enregistrer des favoris. Pour cela, tu fais ton quiz en étant connecté à ton compte et lors du résultat tu appuies sur le bouton "Ajouter à mes favoris"

![resultatFavori](/uploads/ad7bd3b689f8c370bd1d99b4cb9e4b77/250963171_1008536773326024_1989189206099246039_n.png)

Afin de voir la liste dès tes favoris il faut se rendre sur la page quiz en étant connecté à ton compte. En haut à droite il y a un bouton "mes favoris" qui te redirigera sur la liste des boissons que tu as enregistrée.
Si tu souhaites supprimer un favori il suffit d'appuyer sur l'icône de la poubelle à droite de la boisson.

![listeFavori](/uploads/a05a4c57c3a9c2102f93c70894035c65/250921788_402051408295061_7928706027044493974_n.png)
