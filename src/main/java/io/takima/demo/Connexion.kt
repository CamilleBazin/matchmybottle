package io.takima.demo

class Connexion(    var email:String?,
                    var password:String?) {
    constructor(): this(null,null)

    override fun toString(): String {
        return "connexion(email=$email, password=$password)"
    }
}