package io.takima.demo

import javax.persistence.*

@Entity(name = "favoris")

class Favoris (
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Id var id_favoris: Long?,
        @Column(name = "fk_id_user") var fkIdUser: Long?,
        @Column(name = "name_tease") var nameTease: String?,
        @Column(name = "url_tease") var urlTease: String?,
        @Column(name = "com_tease") var comTease: String?,){
    constructor() : this(null, null, null, null, null)

}