package io.takima.demo;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;
import java.util.Optional;
import java.util.Spliterator;

/**
 *
 */
@RequestMapping("/")
@Controller
public class LibraryController {


    //  private final UserDAO userDAO;
    private final TeaseDAO teaseDAO;
    private final UserDAO userDAO;
    private final FavorisDAO favorisDAO;

    //  public LibraryController(UserDAO userDAO) {this.userDAO = userDAO;}
    public LibraryController(TeaseDAO teaseDao, UserDAO userDAO, FavorisDAO favorisDAO) {
        this.teaseDAO = teaseDao;
        this.userDAO = userDAO;
        this.favorisDAO = favorisDAO;
    }

    @GetMapping("/search")
    public String homePage(Model m) {
        m.addAttribute("quizz", new Quizz()); //créer un truc quizz vide prêt à être rempli
        return "accueil";
    }

    @GetMapping("/searchConnecte/{id}")
    public String homePageConnecte(Model m, Model u, @PathVariable("id") int idUser) {
        m.addAttribute("quizz", new Quizz()); //créer un truc quizz vide prêt à être rempli

        Optional<User> user = userDAO.findById((long) idUser);
        user.ifPresent(value -> u.addAttribute("user", value));
        return "accueilConnecte";
    }

    @PostMapping("/search")
    public String resultDrink(@ModelAttribute Quizz quizz,Model m) {

        int id_mood = 0;
        int id_poto = 0;
        int id_thune = 0;

        if (quizz.getMakeParty()) {
            id_mood = 1;
        } else if (quizz.getChill()) {
            id_mood = 2;
        } else if (quizz.getForgetMyNight()) {
            id_mood = 3;
        }


        if (quizz.getCrush()) {
            id_poto = 2;
        } else if (quizz.getAlone()) {
            id_poto = 3;
        } else if (quizz.getFriends()) {
            id_poto = 1;
        }

        if (quizz.getTen()) {
            id_thune = 1;
        } else if (quizz.getTwentyFive()) {
            id_thune = 2;
        } else if (quizz.getRich()) {
            id_thune = 3;
        }


            Tease tease = teaseDAO.findByFkIdMoodAndFkIdPotoAndFkIdThune(id_mood, id_poto, id_thune)
                    .orElseThrow(() -> new RuntimeException("Il nya pas de tease dispo"));
            m.addAttribute("tease",tease);
            return "resultat";

    }

    @PostMapping("/searchConnecte/{id}")
    public String resultDrinkConnecte(@ModelAttribute Quizz quizz,Model m, Model u, @PathVariable("id") int idUser) {

        int id_mood = 0;
        int id_poto = 0;
        int id_thune = 0;

        if (quizz.getMakeParty()) {
            id_mood = 1;
        } else if (quizz.getChill()) {
            id_mood = 2;
        } else if (quizz.getForgetMyNight()) {
            id_mood = 3;
        }


        if (quizz.getCrush()) {
            id_poto = 2;
        } else if (quizz.getAlone()) {
            id_poto = 3;
        } else if (quizz.getFriends()) {
            id_poto = 1;
        }

        if (quizz.getTen()) {
            id_thune = 1;
        } else if (quizz.getTwentyFive()) {
            id_thune = 2;
        } else if (quizz.getRich()) {
            id_thune = 3;
        }


        Tease tease = teaseDAO.findByFkIdMoodAndFkIdPotoAndFkIdThune(id_mood, id_poto, id_thune)
                .orElseThrow(() -> new RuntimeException("Il nya pas de tease dispo"));
        m.addAttribute("tease",tease);

        Optional<User> user = userDAO.findById((long) idUser);
        user.ifPresent(value -> u.addAttribute("user", value));

        return "resultatConnecte";

    }

    @GetMapping("/moins18")
    public String moins18(Model m) {
        return "moins18";
    }


    @GetMapping("/new")
    public String addUserPage(Model m) {
        m.addAttribute("user", new User());
        return "new";
    }

    @PostMapping("/new")
    public RedirectView createNewUser(@ModelAttribute User user, RedirectAttributes attrs) {
        attrs.addFlashAttribute("message", "Utilisateur ajouté avec succès");
        userDAO.save(user);
        return new RedirectView("/");
    }

    @GetMapping("/connexion")
    public String connexion(Model m) {
        m.addAttribute("connexion", new Connexion());
        return "connexion";
    }

    @PostMapping("/connexion")
    public String connectUser(@ModelAttribute Connexion connexion, RedirectAttributes attrs, Model u, Model q) {

        Optional<User> user = userDAO.findByEmailAndPassword(connexion.getEmail(), connexion.getPassword());
        if(user.isPresent()){
            u.addAttribute("user",user.get());
            q.addAttribute("quizz", new Quizz());
            return "accueilConnecte";
        }else{
            attrs.addFlashAttribute("message", "L'email ou le mot de passe ne sont pas bons");
            return "connexion";
        }

    }


    @GetMapping("/favoris/{id}")
    public String favoris(@PathVariable("id") Long idUser, Model m, Model u) {
        List<Favoris> listeFavoris = favorisDAO.findByFkIdUser(idUser);

        m.addAttribute("favoris", listeFavoris);

        System.out.println(listeFavoris);
        Optional<User> user = userDAO.findById( idUser);
        user.ifPresent(value -> u.addAttribute("user", value));

    //    Spliterator<Favoris> listeSplit = listeFavoris.spliterator();


        return "favoris";
    }

    @GetMapping("/ajouterFavoris/{idUser}/{idTease}")
    public RedirectView ajouterFavoris(@PathVariable("idTease") Long idTease, @PathVariable("idUser") Long idUser, Model m) {

        Optional<Tease> teasePotentielle = teaseDAO.findById( idTease);

        if(teasePotentielle.isPresent()){
            Tease tease = teasePotentielle.get();
            Favoris nouveauFavoris = new Favoris();
            nouveauFavoris.setComTease(tease.getTeaseCom());
            nouveauFavoris.setFkIdUser(idUser);
            nouveauFavoris.setNameTease(tease.getTeaseName());
            nouveauFavoris.setUrlTease(tease.getUrlImage());
            favorisDAO.save(nouveauFavoris);

        }else{
            Tease tease = null;
        }

        return new RedirectView("/favoris/{idUser}");
    }

    @GetMapping("/deleteFavoris/{idUser}/{id}")
    public RedirectView deleteFavoris(@PathVariable("id") Long idFavoris, @PathVariable("idUser") Long idUser) {

        favorisDAO.deleteById(idFavoris);

        return new RedirectView("/favoris/{idUser}");
    }




}
