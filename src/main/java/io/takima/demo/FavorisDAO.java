package io.takima.demo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 */
@Repository

public interface FavorisDAO extends CrudRepository<Favoris, Long>{

    @Query("SELECT f FROM favoris f WHERE f.fkIdUser = ?1")
    List<Favoris> findByFkIdUser(Long idUser);

}
