package io.takima.demo

class Quizz(
        var makeParty: Boolean,
        var chill: Boolean,
        var forgetMyNight: Boolean,
        var crush: Boolean,
        var alone: Boolean,
        var friends: Boolean,
        var ten: Boolean,
        var twentyFive: Boolean,
        var rich: Boolean,
) {
    constructor(): this(false, false, false, false, false, false, false, false, false)

    override fun toString(): String {
        return "Quizz(makeParty=$makeParty, chill=$chill, forgetMyNight=$forgetMyNight, crush=$crush, alone=$alone, friends=$friends, ten=$ten, twentyFive=$twentyFive, rich=$rich)"
    }

}