package io.takima.demo

import java.net.URL
import javax.persistence.*

@Entity(name = "tease")

data class Tease (
        @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id var id_tease: Long?,
        @Column(name = "tease_name") var teaseName: String?,
        @Column(name = "tease_com") var teaseCom: String?,
        @Column(name = "url_image") var urlImage: String?,
        @Column(name = "fk_id_mood") var fkIdMood: Int?,
        @Column(name = "fk_id_thune") var fkIdThune: Int?,
        @Column(name = "fk_id_poto") var fkIdPoto: Int?) {
    constructor() : this(null, null, null, null, null,null,null)
}
