package io.takima.demo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 *
 */
@Repository
public interface UserDAO extends CrudRepository<User, Long> {

    Optional<User> findByEmailAndPassword(String email, String password);
}
