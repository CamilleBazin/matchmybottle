package io.takima.demo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 *
 */
@Repository
public interface TeaseDAO extends CrudRepository<Tease, Long> {

    Optional<Tease> findByFkIdMoodAndFkIdPotoAndFkIdThune(Integer fkIdMood, Integer fkIdPoto, Integer fkIdThune);

}
