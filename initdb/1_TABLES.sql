create table users
(
    id bigint auto_increment,
    constraint users_pk
        primary key (id),
    first_name TEXT not null,
    last_name TEXT not null,
    email TEXT not null,
    password TEXT not null
);

create table mood
(
    id_mood bigint auto_increment,
    constraint mood_pk
        primary key (id_mood),
    type_mood TEXT not null
);

create table thune
(
    id_thune bigint auto_increment,
    constraint thune_pk
        primary key (id_thune),
    montant_thune TEXT not null

);
create table poto
  (
      id_poto bigint auto_increment,
      constraint poto_pk
          primary key (id_poto),
      type_poto TEXT not null
  );



create table tease
    (
        id_tease bigint auto_increment,
        tease_name TEXT not null,
        tease_com TEXT not null,
        url_image TEXT not null,
        constraint tease_pk
            primary key (id_tease),
        fk_id_mood bigint,
        fk_id_thune bigint,
        fk_id_poto bigint,
        foreign key (fk_id_mood) references mood (id_mood),
        foreign key (fk_id_thune) references thune (id_thune),
        foreign key (fk_id_poto) references poto (id_poto)
    );

create table favoris
(
    id_favoris bigint auto_increment,
    constraint favoris_pk
        primary key (id_favoris),
    fk_id_user bigint,
    name_tease TEXT not null,
    url_tease TEXT not null,
    com_tease TEXT not null,
    foreign key (fk_id_user) references  users (id)
);
